#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def triangulo_normal(altura):
    triangulo = []
   
    #ciclo para crear el triangulo normal añadiendo / \ o espacios según la posicion i y la altura.
    for i in range (altura):
        if i!=altura-1:
            triangulo.append(" " * (altura-i) + "/" + " " * (i*2) + "\\" )
        else:
            triangulo.append(" " * (altura-i) + "/" + "_" * (i*2) + "\\" )
    
    # se imprime el triangulo.
    for i in range (altura):
        print (triangulo[i])


def triangulo__invertido(altura):    
    triangulo_invertido = []
    print("\n")
    # ciclo para crear el triangulo invertido.
    for i in range (altura):
        if i==0:
            triangulo_invertido.append(" " * i + "\\" + "-" * ((altura-1)*2) + "/" )
        else:
            triangulo_invertido.append(" " * i + "\\" + " " * ((altura-1-i)*2) + "/" )
            
    # se imprime el triangulo invertido.
    for i in range (altura):
        print (triangulo_invertido[i])
    


def triangulo__de__lado(altura):
    triangulo_de_lado = []
    
    # se crea la primera parte del triangulo de lado (parte de arriba) y a su "espejo".
    for i in range (altura):
        triangulo_de_lado.append("|" + " " * i + "\\" + " " * ((altura-1-i)*2) + "/" + " " * i + "|")
    # se crea la segunda parte del triangulo de lado (parte de abajo) y  a su "espejo".
    for i in range (altura):
        triangulo_de_lado.append("|" + " " * (altura-i-1) + "/" + " " * (i*2) + "\\" + " " * (altura-i-1) + "|")

    # se imprime el triangulo de lado.
    for i in range (altura*2):
        print(triangulo_de_lado[i])

# se pide la altura del triangulo.     
altura = int(input("Ingrese la altura del triangulo: "))		
triangulo_normal(altura)
triangulo__invertido(altura)
triangulo__de__lado(altura)
