#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random


def listas(largo):
    matriz = []
    # se crea la matriz 
    for i in range (largo):
        matriz.append([0]*largo)
    # se llena la matriz aleatoriamente
    for i in range (largo):
        for j in range (largo):
            matriz[i][j] = random.randint(0, 10)
    
    imprimir_listas(largo, matriz)



def imprimir_listas(largo, matriz):
    # se imprime la matriz original
    print("MATRIZ ORIGINAL:")
    for i in range (largo):
        for j in range (largo):
            print(matriz[i][j], end=" ")   
        print()
        
    imprimir_listas_invertidas(largo, matriz)
    
    
def imprimir_listas_invertidas(largo, matriz):
    k = 0
    # se imprime la matriz invertida
    print("MATRIZ INVERTIDA")
    for i in range (largo-1,k-1,-1):
        for j in range (largo): 
            print(matriz[i][j], end=" ")
        print()         



largo=int(input("Ingrese el largo de la matriz:"))
listas(largo)
  
