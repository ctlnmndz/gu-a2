#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# función para crear ambas listas
def crear_listas(largo):
    lista_1 = []
    for i in range (largo):
        lista_1.append(str(input("ingrese la palabra:")))
    print("lista 1:")
    print(lista_1)
    
    lista_2 = []
    for i in range (largo):
        lista_2.append(str(input("ingrese la palabra:")))
    print("lista 2:")
    print(lista_2)
    
    listas__juntas(largo,lista_1,lista_2)

def listas__juntas(largo,lista_1,lista_2):
    # se juntan las listas y se crea otra lista para guardar las variables unicas (sin repetirse).
    lista_juntas = lista_1[:] + lista_2[:]
    lista_unica = []
    for i in range (len(lista_juntas)):
	    if lista_juntas[i] not in lista_unica:
		    lista_unica.append(lista_juntas[i]) 
    # se imprime la lista unica.
    print("La listas juntas sin repetir palabras es:")
    print(lista_unica)
    
    lista__unicas(largo,lista_1,lista_2)

def lista__unicas(largo,lista_1, lista_2):
    # se crea una lista donde solo aparecen las palabras de la lista uno pero sin repetirse
    solo_lista_1 = []
   
    for i in range (largo):
	    if lista_1[i] not in solo_lista_1:
		    solo_lista_1.append(lista_1[i]) 
    print("La listas con palabras que solo aparece en la primera lista es:")
    print(solo_lista_1)
    
    # se crea una lista donde solo aparecen las palabras de la lista dos pero sin repetirse
    solo_lista_2 = []
    
    for i in range (largo):
	    if lista_2[i] not in solo_lista_2:
		    solo_lista_2.append(lista_2[i]) 
    print("La listas con palabras que solo aparece en la segunda lista es:")
    print(solo_lista_2)

# se pide el largo de las listas
largo = int(input("Ingrese el largo de las listas:"))
crear_listas(largo)
