#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def ordenar_lista(lista):
    # ciclo para ordenar la lista de menor a mayor.
    for i in range (1,len(lista)):
        for j in range (len(lista)-i):
			# si el valor de la posicion es mayor al de la posicion+1 se intercambian.
            if lista[j]>lista[j+1]:
                aux=lista[j]
                lista[j]=lista[j+1]
                lista[j+1]=aux
    print(lista)

lista=[4,6,8,7,3,1,2,9,5]
ordenar_lista(lista)
