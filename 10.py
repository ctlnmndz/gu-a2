#!/usr/bin/env python3
# -*- coding: utf-8 -*-

string = "¡XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt"
# se elimina las X de la lista
mensaje = string.split("X")
# se invierte el mesaje
mensaje = mensaje[::-1]
# se imprime el mensaje
for i in range(len(mensaje)):
    print(mensaje[i], end="")
