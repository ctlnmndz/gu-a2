#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Función para crear el triangulo.
def triangulo(altura):
    lista= []
    # Se le agrega a la lista los "*" para darle forma al triangulo
    # estos se añadirán (i+1) veces según en la fila que esten y los
    # espacios estaran determinados por (altura-1-i) veces, y esto se 
    # repite al reves para formar el triangulo "espejo".
    for i in range (altura):
        lista.append("*" * (i+1) + " " * (altura-1-i) + 
                     " " * (altura-1-i) + "*" * (i+1))

    # Se imprimir el triangulo.
    for i in range (altura):
        print (lista[i])


altura = int(input("Ingrese la altura del triangulo: "))
triangulo(altura)
