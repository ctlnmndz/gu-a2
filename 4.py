#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random

def lista_menor(largo,k,lista):
	# se crea la lista menor y se añaden los numeros menores a k.
    print("Lista menores a k:")
    menor=[]
    for i in range(largo):
        if lista[i] < k :
            menor.append(lista[i])
    print(menor)

def lista_mayor(largo,k,lista):
	# se crea la lista mayor y se añaden los numeros mayores a k.
    print("Lista mayores a k:")
    mayor=[]
    for i in range (largo):
	    if lista[i] > k:
	        mayor.append(lista[i])
    print(mayor)

def lista_igual(largo,k,lista):
	# se crea la lista igual y se añaden los numeros iguales a k.
    print("Lista iguales a k:")
    igual=[]
    for i in range (largo):
	    if lista[i] == k:
	        igual.append(lista[i])
    print(igual)

def lista_multiplos(largo,k,lista):
	# se crea la lista multiplos y se añaden los numeros multiplos a k.
    print("Lista multiplos a k:")
    multiplos=[]
    for i in range (largo):
        if lista[i]!=0 and lista[i]%k == 0 :
            multiplos.append(lista[i])
    print(multiplos)
    

def lista(largo):
	# se etermina el valor de k aleatoriamente
    k = random.randint(0, 10)
    lista = []

    # se llena la lista aleatoriamente
    for i in range (largo-1):
        lista.append(random.randint(0, 10))
    # se le añade el valor k a la lista
    lista.append(k)
    
    # se imprime la lista
    print("Lista original:")
    print (lista)
    
    lista_menor(largo,k,lista)
    lista_mayor(largo,k,lista)
    lista_igual(largo,k,lista)
    lista_multiplos(largo,k,lista)

# se determina el largo de la lista aleatoriamente.
largo = random.randint(5, 10)
lista(largo)
    	
