#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random

lista = ["Catalina","Hola", "Chao", "Ayer", "Tarde", "Lissette", 
         "Siempre", "Nunca","Mala","Bueno","Temprano", "Chaito"]

# Se crea una variable aleatira para determianar cuantas palabras tendra 
# la lista aleatoria.
cantidad = random.randint(3,6)

# Lista creada aleatoriamente con la lista llamada "lista".
lista_aleatoria = random.sample(lista,cantidad)
print("Lista aleatoria:")
print(lista_aleatoria)

# Se crea la lista inversa de la lista aleatoria.
lista_aleatoria_inversa = lista_aleatoria[::-1]
print("Lista aleatoria inversa:")
print(lista_aleatoria_inversa)

